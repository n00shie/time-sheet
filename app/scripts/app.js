'use strict';

var app = angular.module('cumul8App', ['angularSpinner', 'ngRoute']);

app.config(function ($routeProvider) {
  $routeProvider
    .when('/', {
      templateUrl: 'views/main.html',
      controller: 'MainCtrl'
    })
    .otherwise({
      redirectTo: '/'
    });
});

app.controller('MainCtrl', ['$scope', 'usSpinnerService', function($scope, usSpinnerService) {
  $scope.input = {
    email: '',
    hours: '',
    minutes: '',
    message: '',
    workType: ''
  };

  $scope.submit = false;

  $scope.submitTimeSheet = function(){
    usSpinnerService.spin('spinner-1');
    setTimeout(function(){
      usSpinnerService.stop('spinner-1');
      $scope.nextView();
      $scope.$apply();
    }, 1000);
  };

  $scope.nextView = function(){
    $scope.submit = true;
  };

  $scope.clearForm = function(){
    $scope.input.email = '';
    $scope.input.hours = '';
    $scope.input.minutes = '';
    $scope.input.message = '';
    $scope.input.workType = '';
    $scope.inputForm.$setPristine();
  };

  $scope.startAgain = function(){
    $scope.clearForm();
    $scope.submit = false;
  };
}]);
