# README #

### Setup ###

You can just open app/index.html

or run the full server like so:

```
#!bash

$ sudo npm install
$ grunt server
```

The app will be running at 
```
#!bash

localhost:9000
```


### Spec fulfillment ###

Figure A:

1. This is done implicitly via the input fields and placeholder text.

2. Email validation should work as expected. I decided to split the time input into two number input fields, with minimum values of 0 for both minute and hour values, and a max of 59 for minutes only. If the inputs are invalid, I display an error message underneath the inputs.

3. Satisfied by item 2.

4. Satisfied via a dropdown list. I made the assumption that hours cannot be logged towards multiple tasks, i.e there's only a one-to-one mapping between the number of hours/minutes logged to the type of work done.

5. Pressing "Clear" will clear inputs as expected, and set each field to be pristine (removes error messages, if any).

6. Pressing next moves to next screen as expected.

Figure B:

1. Loading icon is displayed as requested.

2. The next view is displayed after a 1 second delay. No page redirects FTW!

Figure C:

1. Selecting Start Over goes back to first screen, with all inputs cleared.

Bugs:

The number input field is tricky to run validation on. If a user decides to be malicious and enters text into the number fields, then the following behaviours occur. I have not found a straightforward way to fix this, I suspect I can probably do it with a custom directive, but the amount of work to get that is probably not worth it at this time.

  * Entering a non-number text into the number fields (Hours and Minutes) does not trigger the error message to show up.

* Pressing “Clear" does not seem to clear the number field when non-number text is present.

* Even considering the bugs above, the user would not be able to submit the form if an invalid input is present, so that's a good thing.